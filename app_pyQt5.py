# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 09:09:07 2020

@author: admin
"""
import sys

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import QTime, Qt
from PyQt5.QtCore import QDate
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication,  QWidget, QTableWidget, QTableWidgetItem, QVBoxLayout


nom_viticulteur = []
glob_nom=[]
num_parcelle = []
glob_num=[]
type_cepage = []
glob_cepage=[]
globvar=[]
global_poids=[]
viti_cepa_dic = {}

dic_cepage_id={"1":"pinot noir","2":"merlot    ", "3":"malbec    ", "4":"trousseau", "5":"gamay    ",
               "6":"chardonnay","7":"sauvignon", "8":"grenache", "9":"savagnin", "10":"chenin    "}


class Mangment(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('Presentation.ui', self)
        self.NomDeroulante.setObjectName("NomDeroulante")
        self.NumeroDeroulante.setObjectName("NumeroDeroulante")
        self.Cepage.setObjectName("Cepga")
        self.Poids.setObjectName("Poids")
        self.Enregistrer.setObjectName("Enregistrer")
        self.Visualiser.setObjectName("Visualiser")
        self.Enregistrer.clicked.connect(self.save)
        self.Visualiser.clicked.connect(self.open_visualiser)
        self.NomDeroulante.currentTextChanged.connect(self.on_combobox_changed)
        self.NumeroDeroulante.currentTextChanged.connect(self.on_combobox_cepage_changed)

        with open('producteurs.txt','r') as input_file:
            global glob_nom
            for line in input_file:
                if not line.startswith("nom_viticulteur"):
                    col =line.split(";")
                    nom_viticulteur.append(col[0])
                    num_parcelle.append(col[1])
                    type_cepage.append(col[2])
                
                
            #print(nom_viticulteur)
                
            glob_nom = list(set(nom_viticulteur))
            
        for v in glob_nom:
            self.NomDeroulante.addItem(v)
                
        #print(glob_nom)

     
    def on_combobox_changed(self, myvalue):
        self.NumeroDeroulante.clear()
        indices = [i for i , x in enumerate(nom_viticulteur) if x == myvalue ]
       
       # print( self.NomDeroulante.currentIndex())
        print("combobox changed", myvalue)
        print("All index", indices)
        for ind in indices:
            print("here index ..",ind)
            self.NumeroDeroulante.addItem(num_parcelle[ind])
           
      
     
                
    def on_combobox_cepage_changed(self, myvalue): 
         self.Cepage.clear()
         indices = [i for i , x in enumerate(num_parcelle) if x == myvalue ]
        
        # print( self.NomDeroulante.currentIndex())
         print("combobox changed", myvalue)
         print("All index", indices)
         cepag_list= []
         for ind in indices:
             cepag_list = type_cepage[ind].split(",")
             print("here3", cepag_list)
             for c in cepag_list:
                self.Cepage.addItem(c.strip(" ").strip("\n"))
         

                
            
            
    def save(self):
        global globvar
        now = QDate.currentDate()
        print(now.toString('d.M.yy'))
        time = QTime.currentTime()
        print(time.toString('h.m.s')) 
        
        nom = str( self.NomDeroulante.currentText())
        num = str( self.NumeroDeroulante.currentText())
        cepa= str( self.Cepage.currentText()) 
        poid= int( self.Poids.toPlainText())
        
        mylist = [str(nom),str(num),str(cepa),str(now.toString('d.M.yy')),str(time.toString('h.m.s')),str(poid)]
        globvar.append(mylist)
        print(globvar)
        
                
        #globpoid.append(mylist1)
        self.Poids.clear()
    
    
    def open_visualiser(self):
        Vue2 = Presentation(self) 
        Vue2.show() 
        
  
        
class Presentation(QtWidgets.QMainWindow): 
    def __init__(self, parent=None): 
        super().__init__(parent) 
        uic.loadUi('Presentation1.ui', self) 
        self.PoidsTotal.setObjectName("PoidsTotal")
        self.qtableFromArray(globvar,self.Table)
        self.PoidsTotal.clicked.connect(self.Open_poids)
        self.PoidsTotal.clicked.connect(self.affiche_poids_total)

 
    def qtableFromArray(self, array, qtable):
        print(array)
        nbRow = len(array)
        nbCol = len(array[0])
        print(nbRow,nbCol)
        qtable.setRowCount(nbRow)
        qtable.setColumnCount(nbCol) 
        for i in range(nbRow):
            for j in range(nbCol):
                print(str(array[i][j]))
                qtable.setItem(i,j,QTableWidgetItem(str(array[i][j])))
                
   
    
    
    def affiche_poids_total(self):
        global_poids = []
        viti_cepa_dic = {}
        for i in globvar:
             id0 = str(i[0]+"," + i[2])
             if id0 in viti_cepa_dic:
                 value = viti_cepa_dic[id0]
                 value = value + int(i[5])
                 viti_cepa_dic.update({id0:value})
             else:
                 viti_cepa_dic.update({id0:int(i[5])})
        print("heere mydict", viti_cepa_dic)
        for [key,value] in viti_cepa_dic.items():
            viti = key.split(",")[0]
            cepa = key.split(",")[1]
        
            mylist1=[viti,cepa,str(value)]
            print("here list1 :", mylist1)
            global_poids.append(mylist1)
       
        viti_cepa_dic1={}
        
        with open('Texte.txt','w') as ecriture_fichier:
            for l in globvar:
                value1 = str(l[2])
                if l[0] in viti_cepa_dic1:
                    value1 = viti_cepa_dic1[l[0]] + "," + value1
                    viti_cepa_dic1.update({l[0]:value1})
                else:
                     viti_cepa_dic1.update({l[0]:value1})
                
            line=""
            for [key,value2] in viti_cepa_dic1.items():
                line += key + ":\n" 
                cepa_2_poid = value2.split(",")
                idtocall_list=[]
                for c2p in  cepa_2_poid:
                    idtocall= key + "," + c2p
                    
                    if idtocall not in idtocall_list:
                        line += "\t" +dic_cepage_id[c2p]+"\t" +c2p +"\t" +str( viti_cepa_dic[idtocall])+"\n"
                        idtocall_list.append(idtocall)
                    
                   
            
            ecriture_fichier.write(line)
        return global_poids
                
    def Open_poids(self):
        Vu = Poids_total(self) 
        Vu.show()
        
    print("globpoid",global_poids)
        
        
        
        
   


class Poids_total(QtWidgets.QMainWindow): 
    def __init__(self, parent=None): 
        super().__init__(parent) 
        uic.loadUi("Presentation2.ui",self)
        global_poids2=Presentation.affiche_poids_total(self)
        Presentation.qtableFromArray(self,global_poids2,self.Table1)
      


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv) 
    window = Mangment() 
    window.show()
    sys.exit(app.exec_())
    
    
    